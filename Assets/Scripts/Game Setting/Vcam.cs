﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class Vcam : MonoBehaviour
{
    public CinemachineBrain cine;
    public CinemachineVirtualCamera VirtualCamera;
    private GameObject player, background, canvas, followObj;
    private GameObject[] playerGroup;
    public float parallaxEffect;

    // Start is called before the first frame update
    void Start()
    {
        VirtualCamera = cine.ActiveVirtualCamera as CinemachineVirtualCamera;
        VirtualCamera.m_Lens.OrthographicSize = 250;

        player = GameObject.Find("Player");
        background = GameObject.FindGameObjectWithTag("Background");
        canvas = GameObject.Find("Canvas");

        //First we follow real player
        followObj = player;

        //Get the friendlies
        playerGroup = GameObject.FindGameObjectsWithTag("GroupP");
    }


    // Update is called once per frame
    void Update()
    {
        Zoom();
        UpdateCam();
    }

 
    float touchesPrevPosDifference, touchesCurPosDifference, zoomModifier;

    Vector2 firstTouchPrevPos, secondTouchPrevPos;

    [SerializeField]
    float zoomModifierSpeed = 0.1f;

    void Zoom()
    {

        if (Input.touchCount == 2 && Input.GetTouch(0).position.y > Screen.height / 2)
        {

            Debug.Log("Zooming");

            Touch firstTouch = Input.GetTouch(0);
            Touch secondTouch = Input.GetTouch(1);

            firstTouchPrevPos = firstTouch.position - firstTouch.deltaPosition;
            secondTouchPrevPos = secondTouch.position - secondTouch.deltaPosition;

            touchesPrevPosDifference = (firstTouchPrevPos - secondTouchPrevPos).magnitude;
            touchesCurPosDifference = (firstTouch.position - secondTouch.position).magnitude;

            zoomModifier = (firstTouch.deltaPosition - secondTouch.deltaPosition).magnitude * zoomModifierSpeed;

            if (touchesPrevPosDifference > touchesCurPosDifference)
                VirtualCamera.m_Lens.OrthographicSize += zoomModifier;


            if (touchesPrevPosDifference < touchesCurPosDifference)
                VirtualCamera.m_Lens.OrthographicSize -= zoomModifier;

        }

        VirtualCamera.m_Lens.OrthographicSize = Mathf.Clamp(VirtualCamera.m_Lens.OrthographicSize, 8f, 80f);
    }

    private void UpdateCam()
    {
        if(followObj == null)
        {
            for(int i = 0; i <  playerGroup.Length; i++)
            {
                if(playerGroup[i] != null)
                {
                    followObj = playerGroup[i];
                    break;
                }
            }
            
        }
 
        VirtualCamera.Follow = followObj.transform;
    }
}
