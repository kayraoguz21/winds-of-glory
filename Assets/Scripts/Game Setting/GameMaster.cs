﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMaster : MonoBehaviour
{
    public static int numOfShipsE, numOfShipsP;
    private int shipsGroupP, shipsGroupE;
    public GameObject[] GroupP, GroupE;
    private GameObject AIship, player;

    private void Awake()
    {
        //Group sizes
        shipsGroupE = 4;
        shipsGroupP = 4;

        AIship = GameObject.Find("E0");

        GroupP = new GameObject[shipsGroupP+1];
        GroupE = new GameObject[shipsGroupE+1];

        GroupP[0] = GameObject.Find("P0");
        player = GameObject.Find("P0");

        GroupE[0] = GameObject.Find("E0");

        //Enemy0 must always exist
        InitAIGroups(GroupP, shipsGroupP, GameObject.Find("P0"));
        InitAIGroups(GroupE, shipsGroupE, GameObject.Find("E0"));

        CountShips();
    }

    private void Update()
    {
        //Debug.Log( "Number of active ships: "+numOfShips);
        CountShips();

        if (shipsGroupE < 1 || shipsGroupP < 1)
        {
            GoToMainMenu();
        }
    }

    //Creates and saves a full fleet of AI ships for a group
    private void InitAIGroups(GameObject[] Group, int groupLen, GameObject go)
    {
        int chg;
        if (go.name == "E0")
        {
            chg = 25;
        }
        else
        {
            chg = -25;
        }
       

        for (int i = 1; i < groupLen; i++)
        {
            GameObject AIShipClone = Instantiate(AIship, (go.transform.position + go.transform.up * chg * i), Quaternion.identity);
 
            if (go.tag == "GroupP")
            {
                AIShipClone.tag = "GroupP";
                AIShipClone.name = "P" + (i + 1);
            }
            else if (go.tag == "GroupE")
            {
                AIShipClone.name = "E" + (i + 1);
                AIShipClone.tag = "GroupE";
            }
            Group[i] = AIShipClone;
        }
    }

    //Counts active ships
    public void CountShips()
    {
        shipsGroupE = 0;
        shipsGroupP = 0;

        GameObject[] allGameObjecs = UnityEngine.Object.FindObjectsOfType<GameObject>();

        foreach (GameObject go in allGameObjecs)
        {
            if (go.activeInHierarchy && (go.tag == "GroupP"))
            {
                shipsGroupP++; 
            }
            else if(go.activeInHierarchy && (go.tag == "GroupE")){
                shipsGroupE++;
            }
        }     
    }

    public void GoToMainMenu()
    {
         SceneManager.LoadScene("MainMenu");
    }

}
