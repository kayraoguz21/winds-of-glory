﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public Slider engineers, pumpers, shooters;
    public Text engineersVal, pumpersVal, shootersVal, crewVal;

    public static float engineersNum, pumpersNum, shootersNum, crewNum;

    private void Start()
    {
        engineers.maxValue = 100;
        engineers.minValue = 0;

        pumpers.maxValue = 100;
        pumpers.minValue = 0;

        shooters.maxValue = 100;
        shooters.minValue = 0;
    }

    private void Update()
    {
        CheckInput();
    }

    private void CheckInput()
    {    
        engineersVal.text = "Engineers percentage: %" + engineers.value; 
       
        shootersVal.text = "Shooters percentage: %" + shooters.value; 

        pumpersVal.text = "Pumpers percentage: %" + pumpers.value;

        crewNum = engineers.value + pumpers.value + shooters.value;

        crewVal.text = "Assinged crew: % " + crewNum;

        if (crewNum == 100)
        {
            crewVal.color = Color.green;
        }
        else
        {
            crewVal.color = Color.red;
        }
    }

    public void GoToGameScene()
    {
        if (crewNum == 100)
        {
            engineersNum = engineers.value;
            pumpersNum = pumpers.value;
            shootersNum = shooters.value;
            SceneManager.LoadScene("Game");
        }
    }


}
