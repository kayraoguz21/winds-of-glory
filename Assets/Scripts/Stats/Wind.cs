﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Wind : MonoBehaviour
{
    private float windX, windY, windZ, time, timeW, angleToWind;
    public static float WindSpeed = 7;
    private GameObject wind, player, target;
    private GameObject[] enemyShips;
    public static Quaternion windTargetRotation;
    public float angle;
    public Transform windComp, enemyComp;
  
    // Start is called before the first frame update
    void Start()
    {
        wind = GameObject.FindGameObjectWithTag("Wind");
        player = GameObject.Find("P0");
        enemyShips = GameObject.FindGameObjectsWithTag("GroupE");

        windX = Random.Range(-100f, 100f);
        windY = Random.Range(-100f, 100f);
        windZ = Random.Range(-100f, 100f);

        time = 0;
        timeW = -1;
        angle = 0;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        for (int i = 0; i < enemyShips.Length; i++)
        {
            if(enemyShips[i] != null)
            {
                target = enemyShips[i];
                break;
            }
        }

        time += Time.deltaTime;
        CalcWind();
    }

    private void SetWindAndEnemyDirection()
    {
        Vector3 windOrigin = wind.transform.position;
        Vector3 windHeading = windOrigin + new Vector3(windX, windY, windZ);
    
        windHeading.z = windOrigin.z;

        Vector3 windDir = windHeading - windOrigin;

        Quaternion windRotation = Quaternion.LookRotation(forward: Vector3.forward, upwards: windDir);

        wind.transform.rotation = Quaternion.RotateTowards(wind.transform.rotation, windRotation, 60 * Time.deltaTime);

        windTargetRotation = windRotation;

        if (player != null)
        {
            //Wind Compass only for the player UI
            angleToWind = Vector2.SignedAngle(windDir, player.transform.up);
            windComp.localEulerAngles = new Vector3(0, 0, -angleToWind);

            Vector3 shipPos = player.gameObject.transform.position;
            Vector3 targetPos = target.transform.position;

            //Dont rotate in 3D
            targetPos.z = shipPos.z;

            Vector3 enemyDir = targetPos - shipPos;

            //Enenmy Compass only for the player UI
            float angleToEnenmy = Vector2.SignedAngle(enemyDir, player.transform.up);
            enemyComp.localEulerAngles = new Vector3(0, 0, -angleToEnenmy);
        }
    }

    private void CalcWind()
    {
        if (time - timeW > Random.Range(10, 20) || timeW < 0)
        {
            //Debug.Log("change wind");
            timeW = time;
            windX += Random.Range(-10f, 10f);
            windY += Random.Range(-10f, 10f);
            windZ += Random.Range(-10f, 10f);
        }

        SetWindAndEnemyDirection();
    }
}
