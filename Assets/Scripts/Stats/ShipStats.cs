﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShipStats : MonoBehaviour
{
    //Ship
    public float maxShipSpeed = 5;
    public float currShipSpeed;
    public float defSteeringSpeed = 15;
    public float steeringSpeed;

    //Ship crew
    public float crew = 40;

    public float engineers;
    public float engineersPer = 33;
    public float engineerSpeed = 0.15f;

    public float pumpers;
    public float pumpersPer = 34;
    public float pumperSpeed = 0.25f;

    public float shooters;
    public float shootersPer = 33;
    public float shooterSpeed = 0.25f;

    //Sinking 
    public float holes = 0;
    public float sinkingRate = 5f;

    public float sinkingLoad = 0;
    public float sinkingLoadCap = 100; // max ship volume for sinking

    [HideInInspector]
    public float sinkingSpeed;

    public float pumpingSpeed;
    public float repairSpeed;
    public float loadingSpeed;

    [HideInInspector]
    public bool isHit = false;
    [HideInInspector]
    public bool isSinked = false;
    [HideInInspector]
    public bool assinged;
    [HideInInspector]
    public bool isRam = false;

    //Cannons
    public int cannonsL = 8;
    public int cannonsR = 8;
    public int cannonsrange = 60;
    public int cannonballSpeed = 40;
    public int cannonballs = 100;
    public int cannonLoadingEffort = 10;
}
