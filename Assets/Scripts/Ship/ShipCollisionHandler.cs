﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipCollisionHandler : MonoBehaviour
{
    public float time = 0;
    public float timeRam;

    private void Update()
    {
        time += Time.deltaTime;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "GroupE" || other.tag == "GroupP")
        {
            gameObject.GetComponent<ShipStats>().holes += other.GetComponent<Rigidbody2D>().velocity.magnitude * 0.01f;
            gameObject.GetComponent<ShipStats>().isHit = true;
            gameObject.GetComponent<ShipStats>().isRam = true;
            timeRam = time;
        }
    }
}
