﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonBall : MonoBehaviour
{
    private ShipStats stats;
    public static bool signal = false;
    
    private void Update()
    {
        if(gameObject.GetComponent<Rigidbody2D>().velocity.magnitude < 10f)
        {
            Destroy(gameObject);
            //Debug.Log("cannonball about to stop");
        }
    }
    
    private void OnTriggerEnter2D(Collider2D other)
    {   
        if(other.tag == "GroupP" || other.tag == "GroupE")
        {
            stats = other.GetComponent<ShipStats>();

            if (Random.Range(0, 10) >= Random.Range(0, 10))
            {
                stats.crew -= Random.Range(0, 3);
               if(other.tag == "Player") signal = true;
            }
 
            if (Random.Range(0, 5) > 1)
            {
                stats.holes += 1;
            }

            stats.isHit = true;       
            Destroy(gameObject);
        }   
    }
}
