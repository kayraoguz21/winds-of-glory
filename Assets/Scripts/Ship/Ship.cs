﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Ship : MonoBehaviour
{
    ShipStats  stats;

    //Cannons and Cannonballs
    private GameObject []cannonsL;
    private GameObject [] cannonsR;
    private GameObject cannonPrefabL, cannonPrefabR, cannonballPrefab, cannonFirePrefabL, sail, cannonFirePrefabR, sailPoleClosed, sailPoleOpen;

    //Rigidbody of the ship
    [HideInInspector]
    public Rigidbody2D rbShip;

    [HideInInspector]
    public float currLoadL, currLoadR, maxLoadL, maxLoadR, time, timeL, timeR, angleToWind;
    [HideInInspector]
    public bool leftCannonsLoaded, rightCannonsLoaded, assigned;
         
    void Awake()
    {
        time = 0;
        stats = gameObject.GetComponent<ShipStats>();

        cannonsL = new GameObject[stats.cannonsL];
        cannonsR = new GameObject[stats.cannonsR];

        //Initiate sail
        sailPoleOpen = (GameObject)Resources.Load("Prefabs/SailPoleOpen", typeof(GameObject)); 
        sailPoleClosed = (GameObject)Resources.Load("Prefabs/SailPoleClosed", typeof(GameObject));
        InitSail();

        //Initiate cannons
        cannonPrefabL = (GameObject)Resources.Load("Prefabs/cannonL", typeof(GameObject));
        cannonPrefabR = (GameObject)Resources.Load("Prefabs/cannonR", typeof(GameObject));
        InitCannons();

        //Initiate cannonballs
        cannonballPrefab = (GameObject)Resources.Load("Prefabs/cannon_ball", typeof(GameObject));
        //Initiate cannon fire
        cannonFirePrefabL = (GameObject)Resources.Load("Prefabs/cannonFireL", typeof(GameObject));
        cannonFirePrefabR = (GameObject)Resources.Load("Prefabs/cannonFireR", typeof(GameObject));

        maxLoadR = stats .cannonsR * stats.cannonLoadingEffort;
        maxLoadL = stats .cannonsL * stats.cannonLoadingEffort;

        currLoadL = maxLoadL;
        currLoadR = maxLoadR;

        CalcSpeed();
    }

    private void Start()
    {
        if (gameObject.tag == "GroupP")
        {
            sail.GetComponent<SpriteRenderer>().color = Color.green;
        }
        else if (gameObject.tag == "GroupE")
        {
            sail.GetComponent<SpriteRenderer>().color = Color.red;
        }
    }

    private void FixedUpdate()
    {   
        gameObject.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.up * stats .currShipSpeed);
        time += Time.deltaTime;
        ShipManager();
    }

    private void InitSail()
    {
        sail = Instantiate(sailPoleOpen, (gameObject.transform.position), Quaternion.identity);
        sail.transform.parent = gameObject.transform;

        if(gameObject.tag == "GroupP")
        {
            sail.GetComponent<SpriteRenderer>().color = Color.green;
        }
        else if(gameObject.tag == "GroupE")
        {
            sail.GetComponent<SpriteRenderer>().color = Color.red;
        }
      
    }

    private void InitCannons()
    {
        //initiate right cannons
        for (int i = 0; i < stats .cannonsR; i++)
        {
            GameObject cannon = Instantiate(cannonPrefabR, (gameObject.transform.position + gameObject.transform.up * -i / 2 + gameObject.transform.right * 0.4f), Quaternion.identity);

            cannon.transform.parent = gameObject.transform;

            cannonsR[i] = cannon;
        }

        //initiate left cannons
        for (int i = 0; i < stats .cannonsL; i++)
        {
            GameObject cannon = Instantiate(cannonPrefabL, (gameObject.transform.position + gameObject.transform.up * -i / 2 + gameObject.transform.right * -0.4f), Quaternion.identity);

            cannon.transform.parent = gameObject.transform;

            cannonsL[i] = cannon;
        }
    }

    private void Hit()
    {
        if (stats.isHit)//If the ship is hit calculate current sink and crew stats
        {
            //Set sinking speed
            stats.sinkingSpeed = stats.holes * stats.sinkingRate;

            //Repair holes  
            stats .holes -= stats .repairSpeed * Time.deltaTime;
            if (stats.holes <= 0) stats.holes = 0;

            //Sink the ship 
            stats .sinkingLoad += (stats .sinkingSpeed - stats .pumpingSpeed) * Time.deltaTime;
            if (stats.sinkingLoad <= 0) stats.sinkingLoad = 0;

            if (stats .sinkingLoad <= 0 && stats .holes <= 0) //If Ship is fully repaired
            {
                stats .sinkingLoad = 0;
                stats .holes = 0;
                stats .isHit = false;
            }
            else if (stats .sinkingLoad > stats .sinkingLoadCap) //If Ship is fully sinked
            {
                stats .isSinked = true;
                Destroy(gameObject);
            }
        }
        if (stats.currShipSpeed <= 0) stats.currShipSpeed = 0;
    }
 
    public float timeRam;

    //If sailing in the wind direction increase current ship speed and maneuverability 
    private void CalcSpeed()
    {
        if (stats.isRam)
        {
            gameObject.GetComponent<Rigidbody2D>().angularDrag = 100;
            Debug.Log("Ram!");
            if (gameObject.GetComponent<ShipCollisionHandler>().time - gameObject.GetComponent<ShipCollisionHandler>().timeRam > 2)
            {
                stats.isRam = false;
                gameObject.GetComponent<Rigidbody2D>().angularDrag = 0.5f;
            }
        }
        else
        {
            //gaDebug.Log("No Ram!");
            stats.currShipSpeed = stats.maxShipSpeed - stats.maxShipSpeed * (Quaternion.Angle(gameObject.transform.rotation, Wind.windTargetRotation) * 0.01f + stats.sinkingLoad * 0.1f) * 0.1f;
        }
       
        stats.steeringSpeed = stats.defSteeringSpeed * stats.currShipSpeed * 0.1f;
        //Debug.Log("curr speed" + stats.currShipSpeed);
    }

    private void Crew()
    {
        stats.pumpers = Mathf.Floor(stats.crew * 0.01f * stats.pumpersPer);
        stats.engineers = Mathf.Floor(stats.crew * 0.01f * stats.engineersPer);
        stats.shooters = Mathf.Floor(stats.crew * 0.01f * stats.shootersPer);

        float unassignedEngineers = 0;
        float unassignedPumpers = 0;
        //Re/Distribute crew
        if(stats.holes == 0 && stats.sinkingLoad == 0)
        {
            unassignedEngineers = stats.engineers;
            unassignedPumpers  = stats.pumpers;
        }
        else if(stats.holes == 0)
        {
            unassignedEngineers = stats.engineers;
        }
        else if(stats.sinkingLoad == 0)
        {
            unassignedPumpers = stats.pumpers;
        }
    
        //Calculate crew speeds
        stats.pumpingSpeed =(stats.pumpers - unassignedPumpers) * stats.pumperSpeed;
        stats.repairSpeed = (stats.engineers - unassignedEngineers)* stats.engineerSpeed;
        stats.loadingSpeed = (stats.shooters + unassignedEngineers + unassignedPumpers) * stats.shooterSpeed;
    }

    //Handles damage control and loading cannons
    private void ShipManager()
    {
        Crew();
        CalcSpeed();
        Hit();
        LoadCannons();
       
        if (stats.isSinked)
        {
            if(gameObject.tag == "GroupP")
            {
                GameMaster.numOfShipsP--;
            }

            if(gameObject.tag == "GroupP")
            {
                GameMaster.numOfShipsE--;
            }
        }      
    }

    //Fire
    public void fireL()
    {
        timeL = time;
        int loadedCannons = (int)currLoadL / 10;
        for (int i = 0; i < loadedCannons; i++)
        {
            stats.cannonballs--;
            currLoadL -= 10;

            GameObject cannonball = Instantiate(cannonballPrefab, (cannonsL[i].transform.position + cannonsL[i].transform.right * -0.7f), Quaternion.identity);
            GameObject cannonfire = Instantiate(cannonFirePrefabL, (cannonsL[i].transform.position + cannonsL[i].transform.right * -0.3f), cannonsL[i].transform.rotation);
            cannonfire.transform.parent = cannonsL[i].transform;
            Rigidbody2D rbr = cannonball.GetComponent<Rigidbody2D>();
            float plusSpeed = Random.Range(1f, 1.1f);
            rbr.AddForce(cannonsL[i].transform.right * -stats.cannonballSpeed * plusSpeed, ForceMode2D.Impulse);

            Destroy(cannonfire, 0.3f);
        }
    }

    public void fireR()
    {
        timeR = time;
        int loadedCannons = (int)currLoadR / 10;
        for (int i = 0; i < loadedCannons; i++)
        {
            stats.cannonballs--;
            currLoadR -= 10;

            GameObject cannonball = Instantiate(cannonballPrefab, (cannonsR[i].transform.position + cannonsR[i].transform.right * 0.7f), Quaternion.identity);
            GameObject cannonfire = Instantiate(cannonFirePrefabR, (cannonsR[i].transform.position + cannonsL[i].transform.right * 0.3f), cannonsR[i].transform.rotation);
            cannonfire.transform.parent = cannonsR[i].transform;
           
            Rigidbody2D rbr = cannonball.GetComponent<Rigidbody2D>();
            float plusSpeed = Random.Range(1f, 1.1f);
            rbr.AddForce(cannonsR[i].transform.right * stats.cannonballSpeed * plusSpeed, ForceMode2D.Impulse);

            Destroy(cannonfire, 0.3f);
        }
    }

    public bool isLoadedCannonL()
    {
        if (currLoadL >= maxLoadL * 0.7)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool isLoadedcannonR()
    { 
        if (currLoadR >= maxLoadR * 0.7)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    private void LoadCannons()
    {
        if (currLoadR < maxLoadR)
        {
            currLoadR += Time.deltaTime * stats.loadingSpeed;
        }
        if (currLoadL < maxLoadL)
        {
            currLoadL += Time.deltaTime * stats.loadingSpeed;
        }
    }

    //Steer
    public void turnLeft()
    {
        gameObject.GetComponent<Rigidbody2D>().rotation += -1 * stats .steeringSpeed * Time.deltaTime;
    }

    public void turnRight()
    {
        gameObject.GetComponent<Rigidbody2D>().rotation += +1 * stats .steeringSpeed * Time.deltaTime;
    }
}
