﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class GroupAI : MonoBehaviour
{
    private GameObject canvas, leaderShip, lastShip, currShip;
    private GameObject[] GroupP, GroupE;
    public bool followP, followE, setFollowE, setFollowP;
    void Start()
    {
        followE = true;
        followP = true;
        setFollowE = true;
        setFollowP = true;       
    }

    private void setConvoy( GameObject[] ships, GameObject leaderShip)
    {
        if (leaderShip != null && ships.Length > 1)
        {
            lastShip = leaderShip;

            List<GameObject> currFriendlies = ships.ToList();
          
            for (int i = 0; i < 20; i++)
            {
                float min = float.MaxValue;
               
                for (int j = 0; j < currFriendlies.Count; j++)
                {
                    if (Vector2.Distance(currFriendlies[j].transform.position, lastShip.transform.position) < min)
                    {
                        min = Vector2.Distance(currFriendlies[j].transform.position, lastShip.transform.position);
                        currShip = currFriendlies[j];
                    }
                }

                currShip.GetComponent<AI>().follow = lastShip;
                lastShip = currShip;
            }
        }   
    }

    // Update is called once per frame
    void Update()
    {
        //P0 and E0 are the leaders of the GroupP and GroupE respectively
        if (followE)
        {
            if (GameObject.Find("E0") == null)
            {
                followE = false;
            }
            else
            {
                GroupE = GameObject.FindGameObjectsWithTag("GroupE");
                setConvoy(GroupE, GameObject.Find("E0"));
            }
        }

        if (followP)
        {
            if (GameObject.Find("P0") == null)
            {
                followP = false;
            }
            else
            {
                GroupP = GameObject.FindGameObjectsWithTag("GroupP");
                setConvoy(GroupP, GameObject.Find("P0"));
            }
        }
    }
}
