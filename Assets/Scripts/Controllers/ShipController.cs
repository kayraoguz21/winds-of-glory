﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cinemachine;
public class ShipController : MonoBehaviour
{
    private static bool isPressedL, isPressedR, isPressedFireR, isPressedFireL;

    private GameObject playerShip, enemyShip, canvas;
    private Ship ship;
    private ShipStats  stats;
    public Image crewImage;

    public Slider leftCannonBar, rightCannonBar, sinkingBar, crewBar;

    [HideInInspector]
    public static float angleToWind, timeCrew;
    private float time;
    
    void Start()
    {
        //Get player ship
        playerShip = GameObject.Find("P0");
       
        ship = playerShip.GetComponent<Ship>();
        stats = playerShip.GetComponent<ShipStats>();

        //Initiate cannon load bars
        leftCannonBar.maxValue = stats.cannonsL*10; 
        leftCannonBar.minValue = 0;
        leftCannonBar.value = leftCannonBar.maxValue;

        rightCannonBar.maxValue = stats.cannonsR*10;
        rightCannonBar.minValue = 0;
        rightCannonBar.value = rightCannonBar.maxValue;

        sinkingBar.maxValue = stats.sinkingLoadCap;
        sinkingBar.minValue = 0;
        sinkingBar.value = 0;

        crewBar.maxValue = stats.crew;
        crewBar.minValue = 0;
        crewBar.value = crewBar.maxValue;
        
        //Initiate pressing variables
        isPressedL = false;
        isPressedR = false;
        isPressedFireR = false;
        isPressedFireL = false;

        //Get Player stats
        /*
        stats.engineersPer = MainMenu.engineersNum;
        stats.pumpersPer = MainMenu.pumpersNum;
        stats.shootersPer = MainMenu.shootersNum;
        */
        time = 0;
        timeCrew = float.MaxValue;
        crewImage.enabled = true;
        canvas = GameObject.Find("Canvas");

    }

    // Update is called once per frame
    private void Update()
    {
        time += Time.deltaTime;
        FollowOrder();
        setBars();
        
        if (isPressedL)ship.turnLeft();
        if (isPressedR)ship.turnRight();

        if (isPressedFireL)
        {
            ship.fireL();
            isPressedFireL = false;
        }
        if (isPressedFireR)
        {
            ship.fireR();
            isPressedFireR = false;
        }

        angleToWind = ship.angleToWind;

        //Debug.Log("Crew: " + stats.crew);
    }

    public void onPressL()
    {
        isPressedL = true;
    }

    public void onReleaseL()
    {
        isPressedL = false;
    }

    public void onPressR()
    {
        isPressedR = true;
    }

    public void onReleaseR()
    {
        isPressedR = false;
    }

    public void onPressFireL()
    {    
       isPressedFireL = true;
    }

    public  void onPressFireR()
    {     
       isPressedFireR = true;
    }

    private void setBars()
    {
        rightCannonBar.value = ship.currLoadR;

        leftCannonBar.value = ship.currLoadL;

        sinkingBar.value = stats.sinkingLoad;

        crewBar.value = stats.crew;

        if (CannonBall.signal)
        {
            timeCrew = time;
        }

        if (time - timeCrew < 2 && time - timeCrew >= 0)
        {
            CannonBall.signal = false;
            crewImage.enabled = true;
            crewImage.sprite = (Sprite)Resources.Load<Sprite>("Sprites/skull");
            crewBar.image.color = Color.red;
        }
        else
        {
            crewImage.enabled = false;
            crewBar.image.color = Color.gray;
        }
    }

    int chg = 1;

    //Detects follow order from player
    public void FollowOrder()
    {
        bool result = false;
        float MaxTimeWait = 2;
        float VariancePosition = 1;

        if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            float DeltaTime = Input.GetTouch(0).deltaTime;
            float DeltaPositionLenght = Input.GetTouch(0).deltaPosition.magnitude;

            if (DeltaTime > 0 && DeltaTime < MaxTimeWait && DeltaPositionLenght < VariancePosition)
                result = true;
        }
        if (result)
        {
            if (chg > 0)
            {
                canvas.GetComponent<GroupAI>().followP = true;
                Debug.Log("follow");
                chg *= -1;
            }
            else
            {
                canvas.GetComponent<GroupAI>().followP = false;
                chg *= -1;
            }
        }     
    }
}
