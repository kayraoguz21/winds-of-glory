﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI : MonoBehaviour
{ 
    [HideInInspector]
    public GameObject follow, enemy;

    private GameObject AIship, target;
    private GameObject[] currEnemies;
    private Ship ship;
    private ShipStats stats;
    private GameObject canvas;
    List<GameObject> allShips;

    private float heading, LengthSides, lengthMid, distanceToTarget;
    
    private float engageRotation, time, engagedTime, firingTime, timeTarget;
    private float rangeAngle, up, down;

    private Quaternion targetRotation;

    //AI states
    private bool fitToEngage, engaged, engage, engageL, engageR, isNewEngageSideSet, detect, inRange;

    public float val;

    float chg = 1f;
    private Vector3 originL,aimL,directionL,originR,aimR,directionR, engageOrigin, engageDir;
    private float lower, upper;
    private bool isLeft,isRight, isFollow;
    string enemyTag;
    bool isLeader;

    private void Awake()
    {
        //Ignore itself
        Physics2D.queriesStartInColliders = false;
    }

    private void Start()
    {
        Physics2D.queriesStartInColliders = false;
        //Get the AI ship 
        AIship = gameObject;
        ship = AIship.GetComponent<Ship>();
        stats = AIship.GetComponent<ShipStats>();
       
        //Set AI crew stats
        stats.shootersPer = 33;
        stats.engineersPer = 33;
        stats.pumpersPer = 34;
 
        //Variables
        engageL = false;
        engageR = false;
        engaged = false;
        isNewEngageSideSet = false;
        inRange = false;
        isLeader = false;
        lengthMid = 20;
        LengthSides = 65;
        engagedTime = 0;
        firingTime = 0;
        rangeAngle = 0.018f;
        up = 1f;
        down = -3f;
        timeTarget = -26f;
        
        //Target variables
        enemy = null;
        follow = null;

        //Canvas
        canvas = GameObject.Find("Canvas");
       
        //Set AI data according to its group
        switch (gameObject.tag)
        {
            case "GroupP":
                currEnemies = GameObject.FindGameObjectsWithTag("GroupE");
                isFollow = canvas.GetComponent<GroupAI>().followP;
                enemyTag = "GroupE";
                isLeader = false;
                break;
            case "GroupE":
                currEnemies = GameObject.FindGameObjectsWithTag("GroupP");
                ship.transform.rotation = Quaternion.Euler(0, 0, 180);
                isFollow = canvas.GetComponent<GroupAI>().followE;
                enemyTag = "GroupP";
                if (gameObject.name == "E0")
                {
                    isLeader = true;
                }
                else
                {
                    isLeader = false;
                }
                break;
        }
        
        allShips = GetShips();
    }

    void FixedUpdate()
    {
        time += Time.deltaTime;
        AIBehaviour();
    }

    //Determine if the ship should follow if not leader
    private void DetermineFollow()
    {
        if(!isLeader)
        {
            switch (gameObject.tag)
            {
                case "GroupP":
                    isFollow = canvas.GetComponent<GroupAI>().followP;
                    if (GameObject.Find("P0") == null)
                    {
                        isFollow = false;
                    }
                    break;
                case "GroupE":
                    isFollow = canvas.GetComponent<GroupAI>().followE;
                    if (GameObject.Find("E0") == null)
                    {
                        isFollow = false;
                    }
                    break;
            }
        }
    }

    //Find the closest enemy
    private void GetEnemy()
    {
        float minDist = float.MaxValue;
        if (time - timeTarget > 2 || enemy == null)
        {
            for (int i = 0; i < currEnemies.Length; i++)
            {    
                if (currEnemies[i] != null)
                {
                    if (Vector2.Distance(ship.gameObject.transform.position, currEnemies[i].transform.position) < minDist)
                    {
                            minDist = Vector2.Distance(ship.gameObject.transform.position, currEnemies[i].transform.position);
                            enemy = currEnemies[i];
                    }
                }
            }
                timeTarget = time;
        }
        //Debug.Log("target: " + target.name);     

        if (isFollow && !isLeader)
        {
            target = follow;
        }
        else
        {
            target = enemy;
        }
    }

    //Approach the target
    private void Maneouver()
    {
        if(!isFollow || isLeader)
        {
           
            if (engage)
            {
                distanceToTarget = Vector2.Distance(ship.gameObject.transform.position, target.transform.position);

                if (distanceToTarget <= stats.cannonsrange)
                {
                    inRange = true;

                    if (isLeft && ship.isLoadedCannonL())//if left turn right
                    {
                        heading = 100 * chg;
                        isNewEngageSideSet = true;
                       
                    }
                    else if (isRight && ship.isLoadedcannonR())//if right turn left until aim is taken
                    {
                        heading = 100 * chg;
                        isNewEngageSideSet = true;
                    }
                    else
                    {
                        heading = 0;
                    }
                }
                else
                {
                    heading = 0;
                    engaged = false;
                    inRange = false;
                }

                if (engageRotation < 1 && !engaged)
                {
                    //Debug.Log(engageRotation);
                    engagedTime = time;
                    engaged = true;
                    //Debug.Log("Engaged");
                }
                else
                {
                    engaged = false;
                }

                //If staying too long in the same dir change dir
                if ((time - engagedTime > 3) && (time - firingTime > 3) && engaged)
                {
                    //Debug.Log("Attack maneouver");
                    isNewEngageSideSet = false;
                    chg *= -1;
                }
            }
            else
            {
                if (stats.sinkingLoad >= stats.sinkingLoadCap * 0.8)
                {
                    //Debug.Log("defencive");
                    engage = true;
                }
                else
                {
                    //select retreat dir
                    if (Quaternion.Angle(transform.rotation, targetRotation) > Quaternion.Angle(transform.rotation, Wind.windTargetRotation))
                    {
                        targetRotation = Wind.windTargetRotation;
                    }
                    else
                    {
                        heading = -180 + Random.Range(-45f, 45f);
                    }
                    //Debug.Log("Retreat");
                }
            }
        }
        else
        {
            heading = 0;
        }   
    }

    //Steer the ship
    private void Steer()
    {
        originL = transform.position + transform.right * 2.5f + transform.up * -1.5f;
        aimL = target.transform.position + target.transform.up * -1.5f;
        directionL = aimL - originL;

        originR = transform.position + transform.right * -2.5f + transform.up * -1.5f;
        aimR = target.transform.position + target.transform.up * -1.5f;
        directionR = aimR - originR;

        isLeft = false;
        isRight = false;

        if (Vector2.Distance(target.transform.position, originL) < Vector2.Distance(target.transform.position, originR))
        {
            engageOrigin = originL;
            engageDir = directionL;
            //Debug.Log("side right");
            isRight = true;
        }
        else if (Vector2.Distance(target.transform.position, originL) > Vector2.Distance(target.transform.position, originR))
        {
            engageOrigin = originR;
            engageDir = directionR;
            //Debug.Log("side left");
            isLeft = true;
        }

        ImpactAvoider();

        Vector3 shipPos = ship.gameObject.transform.position;
        Vector3 targetPos = target.transform.position;

        //Dont rotate in 3D
        targetPos.z = shipPos.z; 

        Vector3 vectorToTarget = targetPos - shipPos;

        //Set heading  
        Vector3 rotatedVectorToTarget = Quaternion.Euler(0, 0, heading) * vectorToTarget;

        //Set rotation
        targetRotation = Quaternion.LookRotation(forward: Vector3.forward, upwards: rotatedVectorToTarget);

        engageRotation = Quaternion.Angle(transform.rotation, targetRotation);
  
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, stats.steeringSpeed * Time.deltaTime);
    } 

    //Firíng with detectors
    private void FireBehaviour(string side)
    {
        float val1 = 0;
        float val2 = 0;

        if (distanceToTarget < stats.cannonsrange / 4)
        {
            lengthMid = 10;         
            val = 1.5f;
        }
        else if (distanceToTarget >= stats.cannonsrange / 4)
        {
            lengthMid = 20f;        
            val = 50f;
        }

        if (side.Equals("right")) 
        {
            val1 = 1.5f;
            val2 = 1;
            if (val < 0) val *= -1;
        }else if (side.Equals("left"))
        {
            val1 = -1.5f;
            val2 = -1;
            if (val > 0) val *= -1;
        }

        // down
        Vector3 originR = transform.position + transform.right * val1 + transform.up * down;
        Vector3 directionR = val2 * transform.right + transform.up *rangeAngle;
        RaycastHit2D hitR = Physics2D.Raycast(originR, directionR, LengthSides);

        // up
        Vector3 originUpR = transform.position + transform.right * val1 + transform.up * up;
        Vector3 directionUpR = val2 * transform.right + transform.up *-rangeAngle;
        RaycastHit2D hitUpR = Physics2D.Raycast(originUpR, directionUpR, LengthSides);

        // mid
        Vector3 originMidR = transform.position + transform.right * val + transform.up * -1.3f;
        Vector3 directionMidR = val2 * transform.right;
        RaycastHit2D hitMidR = Physics2D.Raycast(originMidR, directionMidR, lengthMid);

        //Detect(originMidR, directionMidR, hitMidR, lengthMid);
        //bool detectMid = detect;
        Detect(originR, directionR, hitR, LengthSides);
        bool detectUp = detect;
        Detect(originUpR, directionUpR, hitUpR, LengthSides);
        bool detectD = detect;

        bool engageSide = (detectUp && detectD);
       
        if (side.Equals("right"))
        {
            engageR = engageSide;
        }
        else if (side.Equals("left"))
        {
            engageL = engageSide;
        }
        
        Fire();
    }

    //Sets engage angles regarding to relative distance
    private void DetermineEngageAngle(float distance)
    {
        if (distance <= stats.cannonsrange / 2)
        {
            lower = 90;
            upper = 92.5f;
        }
        else
        {
            upper = 95;
            lower = 85;
        }

        lower = 89;
        upper = 91;
    }

    //Áims for a target ship and handles firing
    private void Aim()
    {
        float angle = Vector3.Angle(transform.up, engageDir);
        //Debug.Log("in sight: " + angle);
        DetermineEngageAngle(distanceToTarget);

        if (angle < upper && angle >= lower && distanceToTarget <= stats.cannonsrange / 2)
        {        
            Debug.DrawLine(engageOrigin, engageOrigin + engageDir, Color.red);
           
            if (isRight)
            {
               engageR = true;

            }else if (isLeft)
            {
                engageL = true;
            }
        }
        else
        {
            Debug.DrawLine(engageOrigin, engageOrigin + engageDir, Color.green);
        }

        Fire();

        FireBehaviour("left");
        FireBehaviour("right");
    }

    //Detecs enemies on left and right sides
    private void Detect(Vector3 origin, Vector3 direction, RaycastHit2D hit, float length)
    {
        if (hit.collider != null)
        {
            if (hit.collider.tag == enemyTag)
            {
                Debug.DrawLine(origin, origin + direction * length, Color.red);
               
                detect = true;
            }
            else
            {
                Debug.DrawLine(origin, origin + direction * length, Color.green);
                detect = false;
            }
        }
        else
        {
            Debug.DrawLine(origin, origin + direction * length, Color.green);
            detect = false;
        }
    }

    //Handles collision avoidence and heading 
    private void HeadingSetter()
    {
        Vector3 origin = transform.position + transform.up * 3;
        Vector3 direction = transform.up;
        float len;

        if (isFollow)
        {
            len = 5;
        }
        else
        {
            len = 20;
        }

        RaycastHit2D hit = Physics2D.Raycast(origin, direction, len);

        if (hit.collider != null)
        {
            //Avoid collision
            if ((hit.collider.tag == gameObject.tag ))
            {
                Debug.DrawLine(origin, origin + direction * len, Color.red);
                heading = 180;
            }
            else
            {
                Debug.DrawLine(origin, origin + direction * len, Color.white);
            }
        }
        else
        {
            Debug.DrawLine(origin, origin + direction * len, Color.white);
            Maneouver();
        }
    }

    //Handles left right firing
    private void Fire()
    {
        if (engageL)
        {
           // Debug.Log("Left detected");
            if(ship.isLoadedCannonL())ship.fireL();
            engageL = false;
            firingTime = time;
        }
        
        if (engageR)
        {
           // Debug.Log("Right detected");
            if(ship.isLoadedcannonR())ship.fireR();
            engageR = false;
            firingTime = time;
        }
    }

    //Only if this is the AI leader 
    private void Lead()
    {
        if(gameObject != null)      
        {
            if (stats.cannonsrange < Vector2.Distance(ship.gameObject.transform.position, target.transform.position))
            {
                canvas.GetComponent<GroupAI>().followE = true;
            }
            else
            {
                canvas.GetComponent<GroupAI>().followE = false;
            }   
        }
    }

    //Main AI behavior function
    private void AIBehaviour()
    {
        //If sinking speed more than pumping speed or number of cannballs are 0: or none of the cannons are loaded not fit to engage 
        if ((stats.sinkingSpeed > stats.pumpingSpeed) || (stats.cannonballs == 0))
        {
            fitToEngage = false;
        }
        else
        {
            fitToEngage = true;
        }

        //Main behaviour determiner
        if((engaged && fitToEngage) || (!engaged && fitToEngage))
        {
            engage = true;
        }
        else
        {
            engage = false;
        }
        GetEnemy();
       
        //Only if this is the AI leader 
        if (isLeader)
        {
            Lead();
        }
        else
        {
            DetermineFollow();
        }

        Steer();
        Aim();
    }

    bool impact;

    private List<GameObject> GetShips()
    {
        GameObject[] allGameObjecs = UnityEngine.Object.FindObjectsOfType<GameObject>();
        List<GameObject> allShips = new List<GameObject>();

        foreach (GameObject go in allGameObjecs)
        {
            if (go.activeInHierarchy && ((go.tag == "GroupP") || (go.tag == "GroupE")))
            {
                allShips.Add(go);
            }
        }

        return allShips;
    }

    private float impactTime;

    private void ImpactAvoider()
    {
        float minDist = float.MaxValue;
        GameObject impactShip = null;
       
        for (int i = 0; i < allShips.Count; i++)
        {
            if (allShips[i] != null)
            {
                if(allShips[i] != gameObject && Vector2.Distance(ship.gameObject.transform.position, allShips[i].transform.position) < minDist)
                {
                    minDist = Vector2.Distance(ship.gameObject.transform.position, allShips[i].transform.position);
                    impactShip =  allShips[i];
                }
            }
        }

        impact = false;

        if(impactShip != null)
        {
            if ( Vector2.Distance(ship.gameObject.transform.position, impactShip.transform.position) <= 15)
            {
                Debug.Log("Impact!");
                target = impactShip;
                if(time - impactTime > 1.5f)
                {
                    if (isLeft)
                    {
                        heading = -140;
                        impactTime = time;
                    }
                    else if (isRight)
                    {
                        heading = 140;
                        impactTime = time;
                    }
                }

                impact = true;
            }
            else
            {
                Maneouver();
            }
        }
        else
        {
            Maneouver();
        }
    }
    
    
    private void OnDrawGizmos()
    {
        if (impact)
        {
            Gizmos.color = Color.red;
        }
        else
        {
            Gizmos.color = Color.blue;
        }
        Gizmos.DrawWireSphere(gameObject.transform.position, 10);
    }
}
